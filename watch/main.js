var data = [{
  name: "Regina",
  base: "tomate",
  price_small: 6.5,
  price_large: 9.95,
  image: "https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300"
}, {
  name: "Napolitaine",
  base: "tomate",
  price_small: 6.5,
  price_large: 8.95,
  image: "https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300"
}, {
  name: "Spicy",
  base: "crème",
  price_small: 5.5,
  price_large: 8,
  image: "https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300"
}];
//# sourceMappingURL=main.js.map