const data = [
	{
		name: 'Regina',
		base: 'tomate',
		price_small: 6.5,
		price_large: 9.95,
		image:
			'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300',
	},
	{
		name: 'Napolitaine',
		base: 'tomate',
		price_small: 6.5,
		price_large: 8.95,
		image:
			'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300',
	},
	{
		name: 'Spicy',
		base: 'crème',
		price_small: 5.5,
		price_large: 8,
		image:
			'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300',
	},
];

class Component {
	constructor(tagName, param = '', children = null) {
		this.tagName = tagName;
		this.param = param;
		this.children = children;
	}
	render() {
		let param = '';
		if (this.param.name !== undefined || this.param.value !== undefined)
			param = `${this.param.name}="${this.param.value}"`;
		if (this.children === null) return `<${this.tagName} ${param}/>`;
		if (this.children instanceof Array)
			return `<${this.tagName} ${param}> ${this.children.join('')} </${
				this.tagName
			}>`;
		if (this.children instanceof String)
			return `<${this.tagName} ${param}> ${this.children} </${this.tagName}>`;
		if (this.children instanceof Component)
			return `<${this.tagName} ${param}> ${this.children.render()} </${
				this.tagName
			}>`;
		return '';
	}
}
class img extends Component {
	constructor(param) {
		super('img', param);
	}
}
const title = new Component('h1', null, ['La', ' ', 'carte']);
document.querySelector('.pageTitle').innerHTML = title.render();
